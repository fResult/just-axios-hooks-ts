import React, { useState } from "react";
import "./App.css";
// import TaskItem from './pages/TaskItem'
import TaskList from './pages/TaskList'

function App() {
  return (
    <div className="App">
      {/*<TaskItem taskId="604626bfb1d875001738d0dd" />*/}
      <TaskList />
    </div>
  );
}

export default App;
