import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { Todo } from '../types/Todo'
import { TOKEN } from '../../env'

type TaskItemProps = {
  taskId: string
}

function TaskItemOld({ taskId }: TaskItemProps) {
  const [error, setError] = useState('')
  const [loading, setLoading] = useState(false)
  const [task, setTask] = useState<Partial<Todo>>({})

  useEffect(() => {
    fetchOneTask().then()
  }, [])

  async function fetchOneTask(): Promise<void> {
    try {
      setLoading(true)
      const { data: respBody } = await axios.get(
        `https://api-nodejs-todolist.herokuapp.com/task/${taskId}`,
        {
          headers: {
            Authorization: `Bearer ${/*process.env.*/ TOKEN}`
          }
        }
      )

      setTask(respBody.data)
    } catch (err) {
      setError(
        `❌ Error: ${
          err.response?.data.message
            ? err.response.data.message
            : err.response.data.error
        }`
      )
    } finally {
      setLoading(false)
    }
  }
  return (
    <div className="task-page">
      <h1>Task</h1>
      {error ? (
        <p style={{ fontSize: '1.3rem', color: 'crimson' }}>{error}</p>
      ) : loading ? (
        <p style={{ fontSize: '1.3rem' }}>Task is loading...</p>
      ) : (
        <div style={{ listStyle: 'none' }}>
          <label style={{ display: 'block' }}>
            Task: <input disabled value={task.description} />
          </label>
          <label style={{ display: 'block' }}>
            Done?:{' '}
            <select value={task.completed ? 0 : 1} onChange={() => {}}>
              <option value={0}>Yes</option>
              <option value={1}>No</option>
            </select>
          </label>
        </div>
      )}
    </div>
  )
}

export default TaskItemOld
