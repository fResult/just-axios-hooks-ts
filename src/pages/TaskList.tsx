import React from 'react'
import useAxios from 'axios-hooks'
import { Todo } from '../types/Todo'
import { TOKEN } from '../../env' // Error: TOKEN is not defined. please declare TOKEN with jwt-token firstly

function TaskList() {
  const [
    { data: respBody, loading, error, response }
  ] = useAxios({
    url: `/task`,
    method: 'GET',
    headers: {
      Authorization: `Bearer ${/*process.env.*/TOKEN}`
    }
  })

  const { data: tasks }: { data: Array<Todo> } = respBody || {}

  return (
    <div className="task-list-page" style={{ textAlign: 'center' }}>
      <h1>Tasks need to be done</h1>
      {error ? (
        <p style={{ fontSize: '1.3rem', color: 'crimson' }}>
          {error?.response?.data?.message
            ? error?.response?.data?.message
            : error?.response?.data?.error}
        </p>
      ) : loading ? (
        <p style={{ fontSize: '1.3rem' }}>Task is loading...</p>
      ) : (
        <ul style={{ listStyle: 'none', padding: '0 20px' }}>
          {tasks.map((task) => (
            <li
              key={task._id}
              style={{
                padding: '0.5rem 0',
                width: 350,
                lineHeight: 1.2,
                backgroundColor: 'hsl(240, 8%, 93%)',
                userSelect: 'none',
                border: '1px solid hsl(240, 8%, 70%)',
                borderRadius: 2,
                marginBottom: 7,
                color: '#333'
              }}
            >
              {task.description}
            </li>
          ))}
        </ul>
      )}
    </div>
  )
}

export default TaskList
