import React, { useEffect, useState } from 'react'
import useAxios from 'axios-hooks'
import { Todo } from '../types/Todo'
import { TOKEN } from '../../env'

type TaskItemProps = {
  taskId: string
}

function TaskItem({ taskId }: TaskItemProps) {
  const [
    { data: respBody, loading, error, response },
    executeFetchTask
  ] = useAxios({
    url: `/task/${taskId}`,
    method: 'GET',
    headers: {
      Authorization: `Bearer ${TOKEN}`
    }
  })

  const { data: task }: { data: Todo } = respBody || {}

  return (
    <div className="task-page">
      <h1>Task</h1>
      {error ? (
        <p style={{ fontSize: '1.3rem', color: 'crimson' }}>
          {error?.response?.data?.message
            ? error?.response?.data?.message
            : error?.response?.data?.error}
        </p>
      ) : loading ? (
        <p style={{ fontSize: '1.3rem' }}>Task is loading...</p>
      ) : (
        <div>
          <label style={{ display: 'block' }}>
            Task: <input disabled value={task?.description} />
          </label>
          <label style={{ display: 'block' }}>
            Done?:
            <select value={task?.completed ? 0 : 1} onChange={() => {}}>
              <option value={0}>Yes</option>
              <option value={1}>No</option>
            </select>
          </label>
        </div>
      )}
    </div>
  )
}

export default TaskItem
